#!/bin/sh

# gbp-helpers/get-dists

set -eu

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    exit 1
}

################################################################################
################################################################################
################################################################################

OSID="${OSID:-$(. /etc/os-release && echo "${ID}")}"
case "${OSID}" in
    debian)
        if ! command -v debian-distro-info >/dev/null 2>&1; then
            die missing command: debian-distro-info
        fi
        {
            echo stable
            echo testing
            debian-distro-info --supported \
                | grep -Fxv "$(debian-distro-info --devel)" \
                | grep -Fxv 'experimental'
        } | sort
        ;;
    ubuntu)
        if ! command -v ubuntu-distro-info >/dev/null 2>&1; then
            die missing command: ubuntu-distro-info
        fi
        {
            ubuntu-distro-info --supported
            ubuntu-distro-info --supported-esm
        } | sort | uniq -c | awk '/2/{print $NF}'
        ;;
    *)
        die "unsupported OSID:" "${OSID}"
        ;;
esac
