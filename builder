#!/bin/sh

# gbp-helpers/builder

set -euvx

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    exit 1
}

################################################################################
################################################################################
################################################################################

if [ -z "${DEB_SIGN_KEYID-}" ]; then
    die undefined/empty: DEB_SIGN_KEYID
fi

# Override the sbuild config file via environment variable.
SBUILD_CONFIG="${here}/sbuild.conf"
export SBUILD_CONFIG

################################################################################

# These are explicitly added to the '$environment_filter' in
# 'gbp-helpers/sbuild.conf'. They must be discovered here so that we can see
# them during the execution of 'gbp-helpers/setup-commands'.
#
# These assignments were heavily inspired by the 'dpkg-dev' file
# '/usr/share/dpkg/pkg-info.mk' which does something similar.
DEB_SOURCE="${DEB_SOURCE:-$(dpkg-parsechangelog -SSource)}"
DEB_VERSION="${DEB_VERSION:-$(dpkg-parsechangelog -SVersion)}"
DEB_DISTRIBUTION="${DEB_DISTRIBUTION:-$(dpkg-parsechangelog -SDistribution)}"
SOURCE_DATE_EPOCH="${SOURCE_DATE_EPOCH:-$(dpkg-parsechangelog -STimestamp)}"
export DEB_SOURCE
export DEB_VERSION
export DEB_DISTRIBUTION
export SOURCE_DATE_EPOCH

################################################################################

export >&2

################################################################################

# Invoke 'sbuild' with some fancy arguments.
#
#
# for details, see man 1 sbuild, section EXTERNAL_COMMANDS
#
# use --pre-build-commands to copy a script from host to chroot; tar preserves
# file mode bits
#
# use --chroot-setup-commands to invoke the copied script
#
# Here are the environment variables seen by the '--pre-build-commands'
# (example rapidplan build):
#
#     export DEB_BUILD_OPTIONS='parallel=12 nocheck'
#     export DEB_BUILD_PROFILES='parallel=12 nocheck'
#     export LANG='en_US.UTF-8'
#     export LC_ADDRESS='en_US.UTF-8'
#     export LC_ALL='C.UTF-8'
#     export LC_IDENTIFICATION='en_US.UTF-8'
#     export LC_MEASUREMENT='en_US.UTF-8'
#     export LC_MONETARY='en_US.UTF-8'
#     export LC_NAME='en_US.UTF-8'
#     export LC_NUMERIC='en_US.UTF-8'
#     export LC_PAPER='en_US.UTF-8'
#     export LC_TELEPHONE='en_US.UTF-8'
#     export LC_TIME='en_US.UTF-8'
#     export OLDPWD='/home/neil/code/rapidplan'
#     export PWD='/home/neil'
#     export SHELL='/bin/sh'
#
# I don't know why 'sbuild' (during the pre-build) does a 'cd' from the project
# directory to my '$HOME', but I will assume that this happens all the time,
# every time. To wit, within the "tar-tar" pre-build command, note these
# implicit assumptions:
#
#     $PWD == $HOME
#
#     $here == $HOME/gbp
#
nice -n19 sbuild \
    --pre-build-commands \
    'tar -C ./ -cf- ./gbp/setup-commands | %SBUILD_CHROOT_EXEC sh -c "tar -C /tmp/ -xf-"' \
    --chroot-setup-commands \
    '/tmp/gbp/setup-commands' \
    "$@"

exit "$?"
