#!/bin/sh

# gbp-helpers/install.sh

set -eu

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"
tmpdir="$(
    mktemp -dt "${whatami}.XXXXXX" \
        | xargs -I{} sh -c "chmod 1777 {} && echo {}"
)"
readonly tmpdir="${tmpdir}"
export TMPDIR="${tmpdir}"

cleanup() {
    status="$?"
    rm -rf "${tmpdir}" || true
    return "${status}"
}
trap cleanup EXIT

log() { echo "${whatami}[$$]: $*" >&2; }
error() { log "ERROR: $*"; }
warning() { log "WARNING: $*"; }
info() { log "INFO: $*"; }
die() {
    error "$*"
    usage >&2
    exit 1
}

################################################################################
################################################################################
################################################################################

ln -vnsrf "${here}"/home.gbp.conf "${HOME}"/.gbp.conf
