gbp-helpers
===========

# What?

Things that make `git-buildpackage` easier to use.

# Why?

Because `git-buildpackage` has lots of configurable bits and reaches into a lot
of other tools (which also have a lot of configurable bits).

# How?

This how you get it:

```
$ git clone https://gitlab.com/realtime-neil/gbp-helpers.git ~/gbp
```

All the subsequent instructions assume `~/gbp` is the git worktree. If you made
a different choice, then adjust accordingly.

If you want to use the included `home.gbp.conf`, then do this:

```
$ ln -vnsrf ~/gbp/home.gbp.conf ~/.gbp.conf
'/home/neil/.gbp.conf' -> 'gbp/home.gbp.conf'
```

Or you can run the `install.sh` script which does that part for you.

# Tell Me More

This is a collection of convenience scripts and config files that attempts to
automate some of the chores inherent to setting up your host for Debian
packaging. To do that, I assume the following:

* You have `apt-cacher-ng` running on http://localhost:3142 .

* You are reasonably proficient with `git-buildpackage`.

* You want to build your packages with `sbuild`.

* You want to test your packages with `autopkgtest` via
  `autopkgtest-virt-qemu`.

## Hello, `apt-cacher-ng`.

You're going to be repeatedly downloading a _lot_ of packages, so it makes
sense to use a caching proxy. Install `apt-cacher-ng` on your host like this:

```
# apt-get install apt-cacher-ng
```

With `apt-cacher-ng` installed, tell your host's `apt-config` to start using it
like this:

```
# tee /etc/apt/apt.conf.d/99apt-cacher-ng <<'EOF'
Acquire::http::Proxy "http://localhost:3142";
EOF
```

With that `man 5 apt.conf` file in place, confirm that it is working like this:

```
$ apt-config dump Acquire::http::Proxy
Acquire::http::Proxy "http://localhost:3142";
```

Confirm that the `apt-cacher-ng` service unit is working like this:
```
$ systemctl status apt-cacher-ng
* apt-cacher-ng.service - Apt-Cacher NG software download proxy
     Loaded: loaded (/lib/systemd/system/apt-cacher-ng.service; enabled; vendor preset: enabled)
    Drop-In: /etc/systemd/system/apt-cacher-ng.service.d
             `-override.conf
     Active: active (running) since Tue 2023-01-24 13:18:15 UTC; 4h 33min ago
   Main PID: 2654825 (apt-cacher-ng)
      Tasks: 9 (limit: 18865)
     Memory: 92.1M
     CGroup: /system.slice/apt-cacher-ng.service
             `-2654825 /usr/sbin/apt-cacher-ng -c /etc/apt-cacher-ng ForeGround=1

Jan 24 13:18:15 syn systemd[1]: Starting Apt-Cacher NG software download proxy...
Jan 24 13:18:15 syn systemd[1]: Started Apt-Cacher NG software download proxy.
```

Confirm that the `apt-cacher-ng` is proxying `apt` by watching its log file in
one terminal...

```
$ tail -f /var/log/apt-cacher-ng/apt-cacher.log
```

...while invoking `apt` in another terminal:
```
$ sudo apt-get -y update
```

If `apt-cacher-ng` is working as intended, then the log will show entries like
this:

```
1674583162|O|214|127.0.0.1|ppa.launchpad.net/realtime-robotics/phoxi/ubuntu/dists/focal/InRelease
1674583162|O|212|127.0.0.1|ppa.launchpad.net/realtime-robotics/ros/ubuntu/dists/focal/InRelease
1674583162|O|216|127.0.0.1|ppa.launchpad.net/realtime-robotics/rtlinux/ubuntu/dists/focal/InRelease
1674583162|O|220|127.0.0.1|ppa.launchpad.net/realtime-robotics/third-party/ubuntu/dists/focal/InRelease
1674583163|O|200|127.0.0.1|security.ubuntu.com/ubuntu/dists/focal-updates/InRelease
1674583163|O|199|127.0.0.1|uburep/dists/focal-updates/InRelease
1674583163|O|194|127.0.0.1|packages.ros.org/ros2/ubuntu/dists/focal/InRelease
1674583163|O|205|127.0.0.1|realsense/dists/focal/InRelease
1674583163|O|194|127.0.0.1|deb.nodesource.com/node_16.x/dists/focal/InRelease
1674583163|O|216|127.0.0.1|ppa.launchpad.net/realtime-robotics/trac-ik/ubuntu/dists/focal/InRelease
```

## Hello, `sbuild`.

You're going to be using `sbuild` and `schroot` to create, maintain, and use
secure `chroot`s. Install `sbuild` and `schroot` on your host like this:

```
# apt-get install schroot sbuild
```

With that done, add your login user account to the `sbuild` group. Do this
because, while `sbuild` will be invoked via `sudo` to create `root`-owned
`chroot`s, each package is built in a directory tree owned by `sbuild:sbuild`.

To generate and/or maintain the `chroot`s needed by `sbuild`, use the
`sbuild-maintain` script in this directory. This script supports Ubuntu and
Debian and will create/update a `chroot` for each supported release of your
host OS.

Invoking

```
$ ~/gbp/sbuild-maintain
```

on a `amd64` host running Ubuntu Jammy will create/maintain `chroot`s in the
following locations:

```
$ ls -l /srv/chroot
total 12
drwxr-xr-x 22 root root 4096 Mar 19  2021 bionic-amd64-sbuild
drwxr-xr-x 22 root root 4096 Mar 19  2021 focal-amd64-sbuild
drwxr-xr-x 18 root root 4096 Dec 13  2021 jammy-amd64-sbuild
```

Invoking `sbuild-maintain` on a `amd64` host running Debian Bookworm will
create/maintain `chroot`s in the following locations:

```
$ ls -l /srv/chroot
total 24
drwxr-xr-x 18 root root 4096 Nov 23 10:15 bookworm-amd64-sbuild
drwxr-xr-x 22 root root 4096 Aug  4 14:28 bullseye-amd64-sbuild
drwxr-xr-x 22 root root 4096 Aug  4 14:27 buster-amd64-sbuild
drwxr-xr-x 22 root root 4096 Aug  4 14:31 sid-amd64-sbuild
drwxr-xr-x 22 root root 4096 Jan 11 16:24 stable-amd64-sbuild
drwxr-xr-x 22 root root 4096 Jan 11 16:26 testing-amd64-sbuild
```

## Hello, `autopkgtest`.

In brief, `autopkgtest` allows a Debian package maintainer to declare and run
tests on an installed binary package. The binary package under test can be
installed in several different kinds of "virtualization provider".

You're going to use `autopkgtest` with its "qemu" virtualization
provider. Install `autopkgtest` and `qemu-system` on your host like this:

```
# apt-get install autopkgtest qemu-system
```

With that done, use the `qemu-maintain` script to generate the qemu images you
will need. Invoking

```
$ ~/gbp/qemu-maintain
```

on a `amd64` host running Ubuntu Jammy will create/maintain the following
images:

```
$ ls -l ~/gbp/*.img
-rw-r--r-- 1 neil neil 1125646336 Jan 23 15:27 /home/neil/gbp/autopkgtest-bionic-amd64.img
-rw-r--r-- 1 neil neil  832765952 Jan 23 15:32 /home/neil/gbp/autopkgtest-focal-amd64.img
-rw-r--r-- 1 neil neil 1147338752 Jan 23 15:37 /home/neil/gbp/autopkgtest-jammy-amd64.img
```

# References

* http://joeyh.name/blog/entry/upstream_git_repositories/
* https://github.com/agx/git-buildpackage
* https://honk.sigxcpu.org/projects/git-buildpackage/manual-html/
* https://manpages.debian.org/bullseye/dpkg-dev/dpkg-buildpackage.1.en.html
* https://manpages.debian.org/bullseye/git-buildpackage/git-buildpackage.1.en.html
* https://manpages.debian.org/bullseye/sbuild/sbuild.1.en.html
* https://manpages.debian.org/bullseye/sbuild/sbuild.conf.5.en.html
* https://salsa.debian.org/ci-team/autopkgtest
* https://salsa.debian.org/ci-team/autopkgtest/-/blob/master/doc/README.package-tests.rst
* https://salsa.debian.org/ci-team/autopkgtest/-/blob/master/doc/README.running-tests.rst
* https://salsa.debian.org/ci-team/autopkgtest/-/blob/master/doc/README.virtualisation-server.rst
* https://wiki.debian.org/PackagingWithGit
* https://wiki.debian.org/sbuild
* https://www.eyrie.org/~eagle/notes/debian/git.html
